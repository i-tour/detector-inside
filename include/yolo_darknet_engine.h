//
// Created by zhang on 2021/3/15.
//

#ifndef DETECTOR_INSIDE_CPP_YOLO_DARKNET_ENGINE_H
#define DETECTOR_INSIDE_CPP_YOLO_DARKNET_ENGINE_H

#include "assistant.h"
#include "engine.h"
#include "processor.h"
#include "nlohmann/json.hpp"

class YoloDarknetEngine: public Engine {
public:
    YoloDarknetEngine(bool dont_show, bool ext_output, char* config_file, char* data_file, char* weights,
                      double object_thresh, YoloTrtProcessor& processor);
    void capture(Mat& img);
    void inference();
    void processing(promise<nlohmann::json> json_promise, cv::Mat img);
private:
    network* yolo_net;
    vector<char*> class_names;
    map<char*, Scalar> class_colors;
    double object_thresh;

    bool ext_output;

    YoloTrtProcessor& processor;

    BlockQueue<image> darknet_image_queue;
    BlockQueue<vector<Prediction>> object_detections_queue;
};


#endif //DETECTOR_INSIDE_CPP_YOLO_DARKNET_ENGINE_H
