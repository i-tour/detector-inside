//
// Created by ultcrt on 2021/3/3.
//

#ifndef DETECTOR_INSIDE_C___DETECTOR_H
#define DETECTOR_INSIDE_C___DETECTOR_H

#include "assistant.h"
#include "processor.h"
#include "filter.h"
#include "engine.h"
#include "loopback.h"
#include <string>
#include <opencv2/opencv.hpp>
#include <sw/redis++/redis++.h>

class Detector {
public:
//    Detector(char *input_path, bool dont_show, vector<Engine*> engines, char* redis_url,
//                    int width=960, int height=540, char* out_filename=nullptr);
    Detector(int index, int virtual_index, bool dont_show, vector<Engine*> engines, char* redis_url,
             int width=960, int height=540, char* out_filename=nullptr);
    Detector(std::string path, int virtual_index, bool dont_show, vector<Engine*> engines, char* redis_url,
             int width=960, int height=540, char* out_filename=nullptr);
    void run();

private:
    BlockQueue<Mat> frame_queue;
    BlockQueue<int> frame_time_queue;

    int width;
    int height;
    VideoCaptureLoopback cap;

    char* out_filename;
    bool dont_show;

    vector<Engine*> engines;

    sw::redis::Redis redis_server;

    void video_capture();
    void inference();
    void processing();
    static VideoWriter set_saved_video(int fps, char* output_video, Size size);
};


#endif //DETECTOR_INSIDE_C___DETECTOR_H
