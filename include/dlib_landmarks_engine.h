//
// Created by zhang on 2021/3/15.
//

#ifndef DETECTOR_INSIDE_CPP_DLIB_LANDMARKS_ENGINE_H
#define DETECTOR_INSIDE_CPP_DLIB_LANDMARKS_ENGINE_H

#include "assistant.h"
#include "engine.h"
#include "filter.h"
#include <dlib/opencv.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/shape_predictor.h>
#include <dlib/dnn.h>
#include <dlib/data_io.h>
#include <dlib/image_processing.h>
#include "processor.h"
#include "nlohmann/json.hpp"

#define RATIO 1

template <long num_filters, typename SUBNET> using con5d = dlib::con<num_filters,5,5,2,2,SUBNET>;
template <long num_filters, typename SUBNET> using con5  = dlib::con<num_filters,5,5,1,1,SUBNET>;

template <typename SUBNET> using downsampler  = dlib::relu<dlib::affine<con5d<32, dlib::relu<dlib::affine<con5d<32, dlib::relu<dlib::affine<con5d<16,SUBNET>>>>>>>>>;
template <typename SUBNET> using rcon5  = dlib::relu<dlib::affine<con5<45,SUBNET>>>;

using net_type = dlib::loss_mmod<dlib::con<1,9,9,1,1,rcon5<rcon5<rcon5<downsampler<dlib::input_rgb_image_pyramid<dlib::pyramid_down<6>>>>>>>>;


class DlibLandmarksEngine: public Engine {
public:
    DlibLandmarksEngine(bool dont_show, double pixel_min_thresh, double pixel_max_thresh, char* face_detector,
                        char* shape_predictor, Filter& filter, int width, int height);
    void capture(Mat& img) override;
    void inference() override;
    void processing(promise<nlohmann::json> json_promise, cv::Mat img) override;

private:
    double pixel_min_thresh;
    double pixel_max_thresh;

    int w;
    int h;

    dlib::shape_predictor shape_predictor;
    net_type dnn_net;

    vector<vector<Point>> last_landmarks_array;

    Filter& filter;
//    LandmarksProcessor& processor;

    BlockQueue<dlib::matrix<dlib::rgb_pixel>> dlib_image_queue;
    BlockQueue<vector<FaceDetection>> face_detections_queue;

    Mat cam_mat;
    Mat dist_coeffs;

    FacePose get_face_pose(const vector<Point> &landmarks_68);
    vector<Point> landmarks_smooth(vector<Point>& last_landmarks, vector<Point>& cur_landmarks);
    void face_axis_overlay(Mat image, const Point& base_point, const Mat& r_vec, const Mat& t_vec);
};

void draw_points2line(Mat image, const vector<Point>& points);

void draw_points2ring(Mat image, const vector<Point>& points);

void face_landmarks_overlay(const Mat& image, const vector<Point>& landmarks_68);

#endif //DETECTOR_INSIDE_CPP_DLIB_LANDMARKS_ENGINE_H
