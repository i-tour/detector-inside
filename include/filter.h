//
// Created by ultcrt on 2021/3/3.
//

#ifndef DETECTOR_INSIDE_C___FILTER_H
#define DETECTOR_INSIDE_C___FILTER_H

#include <opencv2/opencv.hpp>

using namespace cv;

class Filter {
public:
    virtual ~Filter();
    virtual Point operator() (Point last_point, Point cur_point) = 0;
};

class NoneFilter: public Filter {
public:
    virtual Point operator() (Point last_point, Point cur_point);
};

class EMAFilter: public Filter {
private:
    double a;
public:
    EMAFilter(double a);
    static Point exponential_smoothing(double a, const Point2d& last_point, const Point2d& cur_point);
    virtual Point operator() (Point last_point, Point cur_point);
};

#endif //DETECTOR_INSIDE_C___FILTER_H
