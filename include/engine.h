//
// Created by zhang on 2021/3/15.
//

#ifndef DETECTOR_INSIDE_CPP_ENGINE_H
#define DETECTOR_INSIDE_CPP_ENGINE_H

#include "assistant.h"
#include "nlohmann/json.hpp"
#include <opencv2/opencv.hpp>

class Engine {
public:
    Engine(bool dont_show);
    virtual ~Engine();
    virtual void capture(cv::Mat& img) = 0;
    virtual void inference() = 0;
    virtual void processing(std::promise<nlohmann::json> json_promise, cv::Mat img) = 0;

protected:
    bool dont_show;
};

#endif //DETECTOR_INSIDE_CPP_ENGINE_H
