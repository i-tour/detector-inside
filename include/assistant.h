//
// Created by ultcrt on 2021/3/3.
//

#ifndef DETECTOR_INSIDE_C___TOOLS_H
#define DETECTOR_INSIDE_C___TOOLS_H

#include <opencv2/opencv.hpp>
#include <mutex>
#include <condition_variable>
#include "darknet.h"
#include <dlib/geometry.h>
#include <dlib/image_processing/full_object_detection.h>
#include <tkDNN/tkdnn.h>

#define PI 3.1415926

typedef struct Prediction {
    char* name;
    double prob;
    box bbox;
} Prediction;

typedef struct FacePose {
    std::array<double, 3> euler_angles;
    cv::Mat r_vec;
    cv::Mat t_vec;
} FacePose;

typedef struct FaceDetection {
    dlib::mmod_rect rect;
    std::vector<cv::Point> landmarks;
} FaceDetection;

typedef struct NCNNDetection {
    char* name;
    double prob;
    cv::Rect rect;
} NCNNDetection;

typedef struct NetworkConfig {
    network* net;
    std::vector<char*> class_names;
    std::map<char *, cv::Scalar> colors;
} NetworkConfig;

// Load network configurations
NetworkConfig load_network_tools(char* config_file, char* data_file, char* weight, int batch_size=1);

// Generate random color for each class
std::map<char *, cv::Scalar> generate_class_colors(const std::vector<char *> &classes_names);

// Detect objects in the image
std::vector<Prediction> detect_image(network *net, const std::vector<char*>& classes_names, image im,
                                     double thresh=0.5, double hier_thresh=0.5, double nms=0.45);

// Remove predictions whose probability is less than 0
std::vector<Prediction> remove_negatives(detection* dets, const std::vector<char *> &classes_names, int num);

// Make probability easier to read
std::vector<Prediction> decode_prediction(const std::vector<Prediction>& predictions);

// Print detections in commandline
void print_detections(const std::vector<Prediction> &predictions, bool coordinates);

// Draw detections using darknet rect
void darknet_draw_boxes(const std::vector<Prediction>& predictions, cv::Mat image,
                        const std::map<char *, cv::Scalar> &colors);

// Draw detections using tkDNN rect
void tkdnn_draw_boxes(const std::vector<tk::dnn::box>& predictions, cv::Mat image,
                      const std::vector<char*> &names, const std::map<char *, cv::Scalar> &colors);

// Draw detections using NCNN rect
void ncnn_draw_boxes(const std::vector<NCNNDetection>& predictions, cv::Mat image,
                     const std::map<char *, cv::Scalar> &colors, double object_thresh);

// Convert bbox used in darknet to rectangle used in opencv
cv::Rect darknet_box2cv_rect(box bbox);

// Convert bbox used in tkDNN to rectangle used in opencv
cv::Rect tkdnn_box2cv_rect(const tk::dnn::box& bbox);

// Convert dlib rectangle to opencv rectangle
dlib::rectangle darknet_bbox2dlib_rect(box bbox);

// Convert detection result used in ncnn to rectangle used in opencv
cv::Rect ncnn_box2dlib_rect(const float* det_res, int width, int height);

// Convert dlib rectangle to opencv rectangle
cv::Rect dlib_rect2cv_rect(dlib::rectangle rect, int ratio);

// Convert dlib full_object_detection to opencv vector<Point>
std::vector<cv::Point> dlib_landmarks2cv_landmarks(dlib::full_object_detection dlib_landmarks, int ratio);

// Get millisecond timestamp of current system time
uint64_t time_ns();

// Convert vector<Point> landmarks into vector<array<int, 2>>
std::vector<std::array<double, 2>> cv_landmarks2std_landmarks(const std::vector<cv::Point>& cv_landmarks, int ratio,
                                                           int width, int height);

template <typename T>
class BlockQueue: public std::queue<T> {
private:
    std::mutex q_mutex;
    std::condition_variable not_full_cv;
    std::condition_variable not_empty_cv;
    unsigned int maxsize;
public:
    BlockQueue(unsigned int maxsize=1);
    void put(T obj);
    T get();
};

template<typename T>
BlockQueue<T>::BlockQueue(unsigned int maxsize) {
    BlockQueue<T>::maxsize = maxsize;
}

template<typename T>
void BlockQueue<T>::put(T obj) {
    std::unique_lock<std::mutex> lock(q_mutex);
    if(maxsize >= 1) {
        // maxsize larger than 0 means need to block put
        not_full_cv.wait(lock, [this](){return std::queue<T>::size() < maxsize;});
    }
    else {
        // maxsize less than 0 means no need to block put
        not_full_cv.wait(lock, [this](){return true;});
    }
    std::queue<T>::push(obj);
    not_empty_cv.notify_one();
}

template<typename T>
T BlockQueue<T>::get() {
    std::unique_lock<std::mutex> lock(q_mutex);
    not_empty_cv.wait(lock, [this](){return !std::queue<T>::empty();});
    T ret = std::queue<T>::front();
    std::queue<T>::pop();
    not_full_cv.notify_one();
    return ret;
}


#endif //DETECTOR_INSIDE_C___TOOLS_H
