//
// Created by ultcrt on 2021/3/16.
//

#ifndef DETECTOR_INSIDE_CPP_YOLO_TRT_ENGINE_H
#define DETECTOR_INSIDE_CPP_YOLO_TRT_ENGINE_H

#include "engine.h"
#include "processor.h"
#include "darknet.h"
#include <tkDNN/tkdnn.h>
#include <tkDNN/DarknetParser.h>
#include <tkDNN/Yolo3Detection.h>
#include "nlohmann/json.hpp"

class YoloTrtEngine: public Engine {
public:
    YoloTrtEngine(bool dont_show, bool ext_output, char* trt_file, char* data_file,
                  double object_thresh, int width, int height);
    void capture(Mat& img);
    void inference();
    void processing(promise<nlohmann::json> json_promise, cv::Mat img);

private:
    double object_thresh;
    bool ext_output;

    int w;
    int h;

    vector<char*> class_names;
    map<char*, Scalar> class_colors;
    tk::dnn::Yolo3Detection detector;
//    YoloTrtProcessor processor;

    BlockQueue<Mat> cv_image_queue;
    BlockQueue<vector<tk::dnn::box>> object_detections_queue;
};


#endif //DETECTOR_INSIDE_CPP_YOLO_TRT_ENGINE_H
