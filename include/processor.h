//
// Created by ultcrt on 2021/3/3.
//

#ifndef DETECTOR_INSIDE_C___PROCESSOR_H
#define DETECTOR_INSIDE_C___PROCESSOR_H

#include <opencv2/opencv.hpp>
#include "assistant.h"
#include <dlib/image_processing/frontal_face_detector.h>
#include <tkDNN/tkdnn.h>
#include <tkDNN/DarknetParser.h>
#include <tkDNN/Yolo3Detection.h>

using namespace cv;
using namespace std;

class Processor {
public:
    Processor(bool dont_show);
    virtual ~Processor();

protected:
    bool dont_show;
};

class YoloTrtProcessor: public Processor {
private:
    float hands_off_shift;

public:
    YoloTrtProcessor(bool dont_show, float hands_off_shift);
    virtual void operator() (const Mat& img, int fps, const vector<char *> &names, const vector<tk::dnn::box>& objects);
};

class LandmarksProcessor: public Processor {
private:
    uint64_t yawn_timer=0;
    uint64_t sleepy_timer=0;
    uint64_t eyes_off_road_timer=0;
    unsigned int yawn_counter=0;

    double yawn_thresh;
    double sleepy_thresh;
public:
    LandmarksProcessor(bool dont_show, double yawn_thresh, double sleepy_thresh);
    void operator()(Mat image, int fps, vector<vector<Point>> face_landmarks_array, vector<Rect> face_rects,
            vector<array<double, 3>> face_euler_angels);
};

#endif //DETECTOR_INSIDE_C___PROCESSOR_H
