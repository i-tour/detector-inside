//
// Created by ultcrt on 2021/3/21.
//

#ifndef DETECTOR_INSIDE_CPP_YOLO_NCNN_ENGINE_H
#define DETECTOR_INSIDE_CPP_YOLO_NCNN_ENGINE_H

#include "engine.h"
#include "processor.h"
#include "ncnn/mat.h"
#include "ncnn/net.h"
#include "nlohmann/json.hpp"

class YoloNcnnEngine: public Engine {
public:
    YoloNcnnEngine(bool dont_show, bool ext_output, char* param_file, char* bin_file, char* data_file,
                   double object_thresh, int model_width, int model_height, int cpu_threads,
                   YoloTrtProcessor& processor);
    void capture(Mat& img);
    void inference();
    void processing(promise<nlohmann::json> json_promise, cv::Mat img);
private:
    int model_width;
    int model_height;
    int cpu_threads;

    ncnn::Net detector;
    vector<char*> class_names;
    map<char*, Scalar> class_colors;

    double object_thresh;
    bool ext_output;

    YoloTrtProcessor& processor;

    BlockQueue<ncnn::Mat> ncnn_image_queue;
    BlockQueue<ncnn::Mat> object_detections_queue;
};


#endif //DETECTOR_INSIDE_CPP_YOLO_NCNN_ENGINE_H
