//
// Created by ultcrt on 2021/3/21.
//

#include <tkDNN/tkdnn.h>
#include <tkDNN/DarknetParser.h>

using namespace std;
using namespace tk::dnn;

int main(int argc, char* argv[]) {
    char *cfg = argv[1];
    char *layers = argv[2];
    char *names = argv[3];
    char *target_path = argv[4];

    Network *net = darknetParser(cfg, layers, names);
    //convert network to tensorRT

    auto *netRT = new NetworkRT(net, target_path);

    if(!fileExist(target_path)) {
        cout << "Error Occurs." << endl;
    }

    delete net;
    delete netRT;

    net = nullptr;
    netRT = nullptr;

    return 0;
}