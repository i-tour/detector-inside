#!/bin/bash

if [ ! -d "build" ]; then
  mkdir build
fi
cd build
cmake .. -DDLIB_USE_CUDA=1 -DNCNN_INSTALL_PATH=/home/detector/ncnn/build/install
cmake --build . --config Release -j8
