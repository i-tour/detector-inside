//
// Created by ultcrt on 2021/3/3.
//
// Filters is modified based on https://github.com/610265158/Peppa_Pig_Face_Engine/blob/master/lib/core/LK/lk.py

#include "filter.h"

Point NoneFilter::operator()(Point last_point, Point cur_point) {
    return cur_point;
}

EMAFilter::EMAFilter(double a): a(a) {}

Point EMAFilter::operator()(Point last_point, Point cur_point) {
    return exponential_smoothing(a, last_point, cur_point);
}

Point EMAFilter::exponential_smoothing(double a, const Point2d& last_point, const Point2d& cur_point) {
    return a*cur_point + (1-a)*last_point;
}

Filter::~Filter() = default;
