//
// Created by ultcrt on 2021/3/3.
//

#include "processor.h"
#include <opencv2/opencv.hpp>

#define FACE "face"
#define PHONE "phone"
#define HAND "hand"
#define CIGARETTE "cigarette"

using namespace cv;
using namespace std;

YoloTrtProcessor::YoloTrtProcessor(bool dont_show, float hands_off_shift):
                                           Processor(dont_show),
                                           hands_off_shift(hands_off_shift) {}

void YoloTrtProcessor::operator()(const Mat& img, int fps, const vector<char *> &names, const vector<tk::dnn::box>& objects) {
    // Phone, cigarette, hands off detection
    vector<float> hand_positions;
    bool phone_detected = false;
    bool cigarette_detected = false;
    float driver_face_x = -1;
    float driver_face_bottom = float(img.rows) / 2 + hands_off_shift;
    for(const tk::dnn::box& obj : objects) {
        Rect rect = tkdnn_box2cv_rect(obj);
        char* name = names[obj.cl];
        if(strcmp(name, FACE) == 0) {
            if(rect.x > driver_face_x) {
                // Most right in screen is the driver
                driver_face_bottom = rect.y + float(rect.height) / 2 + hands_off_shift;
                driver_face_x = rect.x;
            }
        }
        else if(strcmp(name, PHONE) == 0) {
            phone_detected = true;
        }
        else if(strcmp(name, HAND) == 0) {
            hand_positions.push_back(rect.y);
        }
        else if(strcmp(name, CIGARETTE) == 0) {
            cigarette_detected = true;
        }
    }
    unsigned int hand_off_counter=0;
    for(float pos: hand_positions) {
        if(pos < driver_face_bottom) {
            // Higher than driver face
            hand_off_counter++;
        }
    }
    if(hand_off_counter >= 2) {
        // Hands off
        putText(img, "Warning: Put Hands on steering wheel!",
                Point(0, 20),
                FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
        cout << "Warning: Put Hands on steering wheel!" << endl;
    }
    if(phone_detected) {
        // Using phone
        putText(img, "Warning: Stop using phone!",
                Point(0, 40),
                FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
    }
    if(cigarette_detected) {
        putText(img, "Message: Cigarette detected.",
                Point(0, 60),
                FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
    }
}

LandmarksProcessor::LandmarksProcessor(bool dont_show, double yawn_thresh, double sleepy_thresh):
Processor(dont_show),yawn_thresh(yawn_thresh), sleepy_thresh(sleepy_thresh) {

}

void LandmarksProcessor::operator()(Mat image, int fps, vector<vector<Point>> face_landmarks_array,
        vector<Rect> face_rects, vector<array<double, 3>> face_euler_angels) {

    // Yawn, sleepy and eyes off road detection
    if(!face_rects.empty()) {
        // Get driver face at most right in screen
        vector<Point> driver_face_landmarks = face_landmarks_array[0];
        Rect driver_face_rect = face_rects[0];
        array<double, 3> driver_euler_angels = face_euler_angels[0];
        for(int face_idx=1; face_idx < face_landmarks_array.size(); face_idx++) {
            if(face_rects[face_idx].x > driver_face_rect.x) {
                driver_face_rect = face_rects[face_idx];
                driver_face_landmarks = face_landmarks_array[face_idx];
                driver_euler_angels = face_euler_angels[0];
            }
        }
        int mouse_width = driver_face_landmarks[54].x - driver_face_landmarks[48].x;
        int mouse_height = driver_face_landmarks[57].y - driver_face_landmarks[51].y;
        double avg_eye_width = double(driver_face_landmarks[39].x - driver_face_landmarks[36].x +
                                      driver_face_landmarks[45].x - driver_face_landmarks[42].x) / 2;
        double avg_eye_height = double(driver_face_landmarks[40].y - driver_face_landmarks[38].y +
                                       driver_face_landmarks[47].y - driver_face_landmarks[43].y) / 2;
        if(mouse_height > yawn_thresh*mouse_width) {
            // Yawn detected
            if(!dont_show) {
                putText(image, "Yawn Detected",
                        Point(driver_face_rect.x-10, driver_face_rect.y+driver_face_rect.height+20),
                        FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
            }
        }
        if(avg_eye_height * sleepy_thresh < avg_eye_width / 10) {
            // Sleepy detected
            if(!dont_show) {
                putText(image, "Sleepy Detected",
                        Point(driver_face_rect.x-10, driver_face_rect.y+driver_face_rect.height+50),
                        FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
            }
        }
        if(driver_euler_angels[0] > 15 || driver_euler_angels[0] < -15
           || driver_euler_angels[1] > 15 || driver_euler_angels[1] < -15) {
            putText(image, "Eyes off road Detected",
                    Point(driver_face_rect.x-10, driver_face_rect.y+driver_face_rect.height+80),
                    FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
        }
    }
}

Processor::Processor(bool dont_show): dont_show(dont_show) {}

Processor::~Processor() = default;
