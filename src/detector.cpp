//
// Created by ultcrt on 2021/3/3.
//

#include "detector.h"
#include <thread>
#include <utility>
#include "nlohmann/json.hpp"

#define DETECT_INSIDE_SIZE 10

using namespace nlohmann;
using namespace std;
using namespace cv;
using namespace sw::redis;

//Detector::Detector(char *input_path, bool dont_show, vector<Engine*> engines, char* redis_url,
//                                 int width, int height, char* out_filename):
//                                 frame_queue(0), cap(input_path),
//                                 engines(std::move(engines)), width(width), height(height),
//                                 out_filename(out_filename), dont_show(dont_show),
//                                 redis_server(redis_url){}

Detector::Detector(string path, int virtual_index, bool dont_show, vector<Engine*> engines, char* redis_url,
                   int width, int height, char *out_filename):
                   frame_queue(0), cap(std::move(path), virtual_index, 1280, 720, 30),
                   engines(std::move(engines)), width(width), height(height),
                   out_filename(out_filename), dont_show(dont_show), redis_server(redis_url){}

Detector::Detector(int index, int virtual_index, bool dont_show, vector<Engine*> engines, char* redis_url,
                   int width, int height, char *out_filename):
        frame_queue(0), cap(index, virtual_index, 1280, 720, 30),
        engines(std::move(engines)), width(width), height(height),
        out_filename(out_filename), dont_show(dont_show), redis_server(redis_url){}

void Detector::run() {
    thread video_capture_thread(&Detector::video_capture, this);
    thread inference_thread(&Detector::inference, this);
    thread processing_thread(&Detector::processing, this);

    video_capture_thread.join();
    inference_thread.join();
    processing_thread.join();
}

void Detector::video_capture() {
    while(cap.isOpened()) {
        // Frame edit
        Mat frame, frame_rgb, frame_resized;
        cap.read_frame(frame);
        cvtColor(frame, frame_rgb, COLOR_BGR2RGB);
        resize(frame_rgb, frame_resized, Size(width, height));

        frame_queue.put(frame_resized);

        // Async capture
        vector<thread> threads;
        for(Engine* engine: engines) {
            threads.emplace_back(&Engine::capture, engine, ref(frame_resized));
        }
        for(thread& thread : threads) {
            thread.join();
        }
    }
    cap.release();
}

void Detector::inference() {
    while(cap.isOpened()) {
        // Timer is designed to avoid time consumed by block queue
        uint64_t frame_prev_time = time_ns();

        // Async inference
        vector<thread> threads;
        for(Engine* engine: engines) {
            threads.emplace_back(&Engine::inference, engine);
        }
        for(thread& thread : threads) {
            thread.join();
        }

        // IPS calculate(Inference per second)
        int frame_time = int(int(time_ns() - frame_prev_time)/1E6);
        if(frame_time < 2) frame_time=2;
        frame_time_queue.put(frame_time);
        cout << "FPS: " << int(1/frame_time) << endl;
    }
    cap.release();
}

void Detector::processing() {
    srandom(3);
    VideoWriter video;
    if(out_filename != nullptr) video = set_saved_video(25, out_filename, Size(width, height));
    while(cap.isOpened()) {
        Mat img = frame_queue.get();

        int frame_time = frame_time_queue.get();

        // Async processing
        vector<thread> threads;
        vector<future<json>> json_futures;

        // Processing
        for(Engine* engine : engines) {
            promise<json> cur_promise;
            json_futures.emplace_back(cur_promise.get_future());
            threads.emplace_back(&Engine::processing, engine, move(cur_promise), img);
        }

        for(thread& thread: threads) {
            // Will cause "terminate called without an active exception" if not join or detach
            thread.join();
        }

        // Integrate json of each thread
        json json_result;
        uint64_t timestamp = time_ns();

        for(future<json>& json_future: json_futures) {
            json_result[to_string(timestamp)].update(json_future.get());
        }

        redis_server.rpush("detect_inside", json_result.dump());

        // Length check
        vector<string> detect_inside;
        redis_server.lrange("detect_inside", 0 ,-1, back_inserter(detect_inside));

        if(detect_inside.size() > DETECT_INSIDE_SIZE) {
            redis_server.blpop("detect_inside");
        }

//        cout << json_result.dump(2) << endl;

//        cout << detect_inside.size() << endl;

        if(out_filename != nullptr) video.write(img);

        if(!dont_show) {
            imshow("Inference", img);
            // Frame time == wait time will cause lag
            if(waitKey(frame_time/2) == 27) break;
        }
    }
    cap.release();
    if(out_filename!= nullptr) video.release();
    destroyAllWindows();
}

VideoWriter Detector::set_saved_video(int fps, char *output_video, Size size) {
    VideoWriter video;
    video.open(output_video, CAP_ANY, fps, move(size));
    return video;
}