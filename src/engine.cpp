//
// Created by zhang on 2021/3/15.
//

#include "engine.h"

Engine::~Engine() = default;

Engine::Engine(bool dont_show): dont_show(dont_show) {}
