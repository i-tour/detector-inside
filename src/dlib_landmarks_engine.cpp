//
// Created by zhang on 2021/3/15.
//

#include "dlib_landmarks_engine.h"

using namespace nlohmann;

DlibLandmarksEngine::DlibLandmarksEngine(bool dont_show, double pixel_min_thresh, double pixel_max_thresh,
                                         char* face_detector, char* shape_predictor, Filter& filter,
                                         int width, int height) :
                                         Engine(dont_show), pixel_min_thresh(pixel_min_thresh), w(width), h(height),
                                         pixel_max_thresh(pixel_max_thresh), filter(filter), last_landmarks_array() {
    // Initialize face detector and shape predictor
    dlib::deserialize(shape_predictor) >> this->shape_predictor;
    dlib::deserialize(face_detector) >> this->dnn_net;

    // Initialize camera
    unsigned int focal_len = width;
    double center_x = double(width) / 2;
    double center_y = double(height) / 2;
    cam_mat = (Mat_<double>(3, 3) << focal_len, 0, center_x, 0, focal_len, center_y, 0, 0, 1);
    dist_coeffs = (Mat_<double>(4, 1) << 0, 0, 0, 0);
}

void DlibLandmarksEngine::capture(Mat& img) {
    Mat resized_img;
    resize(img, resized_img, Size(img.cols/RATIO, img.rows/RATIO));
    dlib::cv_image<dlib::rgb_pixel> dlib_image(resized_img);
    dlib::matrix<dlib::rgb_pixel> dlib_mat;
    dlib::assign_image(dlib_mat, dlib_image);
    dlib_image_queue.put(dlib_mat);
}

void DlibLandmarksEngine::inference() {
    dlib::matrix<dlib::rgb_pixel> dlib_mat = dlib_image_queue.get();
    uint64 prev = time_ns();
    vector<dlib::mmod_rect> face_rects = dnn_net(dlib_mat);
    cout << "Face detector inference time: " << double(time_ns() - prev) / 1E6 << endl;
    vector<FaceDetection> face_detections;

    prev = time_ns();
    for(int rect_idx=0; rect_idx < face_rects.size(); rect_idx++) {
        if(face_rects[rect_idx].ignore) {
            // Skip not confident face
            continue;
        }
        dlib::full_object_detection dlib_landmarks = shape_predictor(dlib_mat, face_rects[rect_idx].rect);
        vector<Point> cur_landmarks = dlib_landmarks2cv_landmarks(dlib_landmarks, RATIO);
        // Last landmarks do not exist, using current landmarks as last one
        if(last_landmarks_array.size() < rect_idx+1) last_landmarks_array.push_back(cur_landmarks);
        vector<Point> last_landmarks = last_landmarks_array[rect_idx];

        // Smooth
        vector<Point> smoothed_landmarks = landmarks_smooth(last_landmarks, cur_landmarks);

        // face_detection[0] => face rect
        // face_detection[1] => face landmarks
        FaceDetection face_detection = {
                face_rects[rect_idx], smoothed_landmarks
        };
        face_detections.push_back(face_detection);
        last_landmarks_array[rect_idx] = smoothed_landmarks;
    }
    cout << "Landmarks inference time: " << double(time_ns() - prev) / 1E6 << endl;
    face_detections_queue.put(face_detections);
}

void DlibLandmarksEngine::processing(promise<json> json_promise, cv::Mat img) {
    vector<FaceDetection> face_detections = face_detections_queue.get();
//    vector<Rect> face_rects;
//    vector<vector<Point>> face_landmarks_array;
//    vector<array<double, 3>> euler_angles_array;
    json result;
    result["face_detect"] = json::array();
    for(FaceDetection& face_detection : face_detections) {
        FacePose face_pose = get_face_pose(face_detection.landmarks);
        Rect face_rect_cv = dlib_rect2cv_rect(face_detection.rect, RATIO);
//        if(!dont_show) {
            // Need to draw things
            face_landmarks_overlay(img, face_detection.landmarks);
            face_axis_overlay(img, face_detection.landmarks[30],
                              face_pose.r_vec, face_pose.t_vec);
            rectangle(img, face_rect_cv, Scalar(0, 255, 0), 2);
            char text[50];
            sprintf(text, "Face %.2f%% Angle: %.2f, %.2f, %.2f",
                    face_detection.rect.detection_confidence*100, face_pose.euler_angles[0],
                    face_pose.euler_angles[1], face_pose.euler_angles[2]);
            putText(img, text,
                    Point(face_rect_cv.x-10, face_rect_cv.y-10),
                    FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
//        }
//        face_rects.push_back(face_rect_cv);
//        face_landmarks_array.push_back(face_detection.landmarks);
//        euler_angles_array.push_back(face_pose.euler_angles);
        json location;
        location["tl"] = json::array();
        location["tl"].push_back(double(face_detection.rect.rect.left()) / w);
        location["tl"].push_back(double(face_detection.rect.rect.top()) / h);
        location["w"] = double(face_detection.rect.rect.width()) / w;
        location["h"] = double(face_detection.rect.rect.height()) / h;
        json each_face = {
                {"euler_angles", face_pose.euler_angles},
                {"landmarks", cv_landmarks2std_landmarks(face_detection.landmarks, RATIO, w, h)},
                {"location", location}
        };
        result["face_detect"].push_back(each_face);
    }
    json_promise.set_value(result);
//    processor(img, 25, face_landmarks_array, face_rects, euler_angles_array);
}

vector<Point> DlibLandmarksEngine::landmarks_smooth(vector<Point> &last_landmarks, vector<Point> &cur_landmarks) {
    vector<Point> smoothed_landmarks;
    for(int mark_idx=0; mark_idx < cur_landmarks.size(); mark_idx++) {
        Point last_point = last_landmarks[mark_idx];
        Point cur_point = cur_landmarks[mark_idx];

        // Pixel distance between last point and current point
        double dis = sqrt(pow(cur_point.x - last_point.x, 2)+pow(cur_point.y - last_point.y, 2));

        // Smooth
        // Too small distance: use last point
        if(dis < pixel_min_thresh) smoothed_landmarks.push_back(last_point);
            // Normal distance: use filter
        else if(dis < pixel_max_thresh) smoothed_landmarks.push_back(filter(last_point, cur_point));
            // Too large distance: user current point
        else smoothed_landmarks.push_back(cur_point);
    }
    return smoothed_landmarks;
}

FacePose DlibLandmarksEngine::get_face_pose(const vector<Point> &landmarks_68) {
    // Reference face 3D model
    Mat ref_points = (
            Mat_<double>(6, 3) <<
                               0.0,         0.0,       0.0,
                    0.0,      -330.0,     -65.0,
                    -165.0,    170.0,    -135.0,
                    165.0,     170.0,    -135.0,
                    -150.0,   -150.0,    -125.0,
                    150.0,    -150.0,    -125.0
    );

    // Real face 2D model
    vector<Point2d> real_points;
    real_points.push_back(landmarks_68[30]);
    real_points.push_back(landmarks_68[8]);
    real_points.push_back(landmarks_68[36]);
    real_points.push_back(landmarks_68[45]);
    real_points.push_back(landmarks_68[48]);
    real_points.push_back(landmarks_68[54]);

    // Estimate face pose
    Mat r_vec, t_vec;
    bool ret = solvePnP(ref_points, real_points, cam_mat, dist_coeffs, r_vec, t_vec);
    // Estimation failed
    if(!ret) return FacePose();

    // Rotation vector to euler angle
    double theta = norm(r_vec, NORM_L2);
    double x, y, w, z;
    w = cos(theta / 2);
    x = sin(theta / 2)*r_vec.at<double>(0, 0) / theta;
    y = sin(theta / 2)*r_vec.at<double>(0, 1) / theta;
    z = sin(theta / 2)*r_vec.at<double>(0, 2) / theta;

    double y_sqr = y * y;
    double pitch = atan2(+2.0 * (w * x + y * z), +1.0 - 2.0 * (x * x + y_sqr));
    double yaw = asin(max(min(+2.0 * (w * y - z * x), 1.0), -1.0));
    double roll = atan2(+2.0 * (w * z + x * y), +1.0 - 2.0 * (y_sqr + z * z));
    // Pitch cannot be used directly
    pitch = pitch > 0.0? PI-pitch : -PI-pitch;

    array<double, 3> euler_angles({pitch*180/PI, yaw*180/PI, roll*180/PI});
    return {euler_angles, r_vec, t_vec};
}

void DlibLandmarksEngine::face_axis_overlay(Mat image, const Point& base_point, const Mat& r_vec, const Mat& t_vec) {
    Mat axis = (Mat_<double>(3, 3) << 100, 0, 0, 0, 100, 0, 0, 0, 100);
    vector<cv::Point2d> axis_projected_points;
    projectPoints(axis, r_vec, t_vec, cam_mat, dist_coeffs, axis_projected_points);
    line(image, base_point, axis_projected_points[0], Scalar(255, 0, 0), 3);
    line(image, base_point, axis_projected_points[1], Scalar(0, 255, 0), 3);
    line(image, base_point, axis_projected_points[2], Scalar(0, 0, 255), 3);
}

void draw_points2line(Mat image, const vector<Point>& points) {
    polylines(image, points, false, Scalar(0, 0, 255), 2);
}

void draw_points2ring(Mat image, const vector<Point>& points) {
    polylines(image, points, true, Scalar(0, 0, 255), 2);
}

void face_landmarks_overlay(const Mat& image, const vector<Point> &landmarks_68) {
    vector<Point> face(landmarks_68.begin(), landmarks_68.begin()+17);
    vector<Point> left_eyebrow(landmarks_68.begin()+17, landmarks_68.begin()+22);
    vector<Point> right_eyebrow(landmarks_68.begin()+22, landmarks_68.begin()+27);
    vector<Point> vertical_nose(landmarks_68.begin()+27, landmarks_68.begin()+31);
    vector<Point> nose_merge_a({landmarks_68[30], landmarks_68[31]});
    vector<Point> nose_merge_b({landmarks_68[30], landmarks_68[35]});
    vector<Point> horizontal_nose(landmarks_68.begin()+31, landmarks_68.begin()+36);
    vector<Point> left_eye(landmarks_68.begin()+36, landmarks_68.begin()+42);
    vector<Point> right_eye(landmarks_68.begin()+42, landmarks_68.begin()+48);
    vector<Point> outer_lips(landmarks_68.begin()+48, landmarks_68.begin()+60);
    vector<Point> inner_lips(landmarks_68.begin()+60, landmarks_68.begin()+68);

    // Draw face
    draw_points2line(image, face);

    // Draw eyebrows
    draw_points2line(image, left_eyebrow);
    draw_points2line(image, right_eyebrow);

    // raw nose
    draw_points2line(image, vertical_nose);
    draw_points2line(image, horizontal_nose);
    draw_points2line(image, nose_merge_a);
    draw_points2line(image, nose_merge_b);

    // Draw eyes
    draw_points2ring(image, left_eye);
    draw_points2ring(image, right_eye);

    // Draw lips
    draw_points2ring(image, outer_lips);
    draw_points2ring(image, inner_lips);
}
