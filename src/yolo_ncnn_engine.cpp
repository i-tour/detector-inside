//
// Created by ultcrt on 2021/3/21.
//

#include "yolo_ncnn_engine.h"

using namespace nlohmann;

void YoloNcnnEngine::capture(Mat &img) {
    ncnn::Mat ncnn_img = ncnn::Mat::from_pixels_resize(img.data, ncnn::Mat::PIXEL_BGR2RGB,
                                                       img.cols, img.rows, model_width, model_height);
    ncnn_image_queue.put(ncnn_img);
}

void YoloNcnnEngine::inference() {
    ncnn::Mat img = ncnn_image_queue.get();
    const float mean_vals[3] = {0.f, 0.f, 0.f};
    const float norm_vals[3] = {1/255.f, 1/255.f, 1/255.f};

    // Normalize input image
    img.substract_mean_normalize(mean_vals, norm_vals);

    // Detecting
    uint64 prev = time_ns();
    ncnn::Mat ncnn_det_in_mat;
    ncnn::Extractor ex = detector.create_extractor();
    ex.set_num_threads(cpu_threads);
    ex.input("data", img);
    ex.extract("output", ncnn_det_in_mat);
    cout << "NCNN inference time: " << double(time_ns() - prev) / 1E6 << endl;

    object_detections_queue.put(ncnn_det_in_mat);
}

void YoloNcnnEngine::processing(promise<json> json_promise, Mat img) {
    ncnn::Mat ncnn_det_in_mat = object_detections_queue.get();

    // Formatting detections
//    vector<NCNNDetection> formatted_object_detections;
//    for (int i = 0; i < ncnn_det_in_mat.h; i++) {
//        const float* ncnn_det = ncnn_det_in_mat.row(i);
//        char *name = class_names[int(ncnn_det[0])-1];
//        NCNNDetection det = {name, ncnn_det[1], ncnn_box2dlib_rect(ncnn_det, result_json.cols, result_json.rows)};
//        formatted_object_detections.push_back(det);
//    }

//    if(!dont_show) {
//        // Need to draw things
//        ncnn_draw_boxes(formatted_object_detections, result_json, class_colors, object_thresh);
//    }
}

YoloNcnnEngine::YoloNcnnEngine(bool dont_show, bool ext_output, char *param_file, char *bin_file, char *data_file,
                               double object_thresh, int model_width, int model_height, int cpu_threads,
                               YoloTrtProcessor &processor): Engine(dont_show), model_width(model_width),
                                                             model_height(model_height), object_thresh(object_thresh), processor(processor),
                                                             ext_output(ext_output), cpu_threads(cpu_threads) {
    metadata md = get_metadata(data_file);
    for(int name_idx=0; name_idx < md.classes; name_idx++) class_names.push_back(md.names[name_idx]);
    class_colors = generate_class_colors(class_names);
    detector.load_param(param_file);
    detector.load_model(bin_file);
}
