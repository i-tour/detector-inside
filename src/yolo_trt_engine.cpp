//
// Created by ultcrt on 2021/3/16.
//

#include <NvOnnxParser.h>
#include "yolo_trt_engine.h"

using namespace nvonnxparser;
using namespace nlohmann;

YoloTrtEngine::YoloTrtEngine(bool dont_show, bool ext_output, char *trt_filepath,
                             char *data_file, double object_thresh, int width, int height):
Engine(dont_show), ext_output(ext_output), object_thresh(object_thresh), w(width), h(height) {
    metadata md = get_metadata(data_file);
    for(int name_idx=0; name_idx < md.classes; name_idx++) class_names.push_back(md.names[name_idx]);
    class_colors = generate_class_colors(class_names);
    detector.init(trt_filepath, class_names.size());
}

void YoloTrtEngine::capture(Mat& img) {
    cv_image_queue.put(img);
}

void YoloTrtEngine::inference() {
    Mat img = cv_image_queue.get();
    vector<Mat> detector_img;
    detector_img.push_back(img);
    vector<tk::dnn::box> detection_boxes;

    uint64 prev = time_ns();
    detector.update(detector_img);
    cout << "tkDNN inference time: " << double(time_ns() - prev) / 1E6 << endl;

    object_detections_queue.put(detector.detected);
}

void YoloTrtEngine::processing(promise<json> json_promise, Mat img) {
    vector<tk::dnn::box> object_detections = object_detections_queue.get();
    json result;
    result["class_detect"] = json::array();
    for(const tk::dnn::box& object_detection: object_detections) {
        json each_obj = {
                {"object_class", class_names[object_detection.cl]},
                {"location", {
                    {"tl", {double(object_detection.x) / w, double(object_detection.y) / h}},
                    {"w", double(object_detection.w) / w},
                    {"h", double(object_detection.h) / h}
                }}
        };
        result["class_detect"].push_back(each_obj);
    }
    json_promise.set_value(result);

//    if(!dont_show) {
//        vector<tk::dnn::box> above_thresh;
//        for(const tk::dnn::box& prediction: object_detections) {
//            if(prediction.prob >= object_thresh) {
//                above_thresh.push_back(prediction);
//            }
//        }
        processor(img, 25, class_names, above_thresh);
        tkdnn_draw_boxes(above_thresh, img, class_names, class_colors);
//    }
}
