//
// Created by ultcrt on 2021/3/3.
//

#include "memory"
#include "detector.h"
#include "yolo_darknet_engine.h"
#include "dlib_landmarks_engine.h"
#include "yolo_trt_engine.h"
#include "yolo_ncnn_engine.h"

using namespace std;

int main(int argc, char* argv[]) {
    Filter* filter;
    char* out_file= nullptr;
    map<string, char*> args;
    string arg_names[] = {
            "executable",
            "config_file",
            "data_file",
            "weights",
            "face_detector",
            "shape_predictor",
            "input_method",
            "input",
            "processor_arg",
            "use_filter",
            "filter_arg",
            "width",
            "height",
            "obj_thresh",
            "pix_min",
            "pix_max",
            "use_out_file",
            "out_filename",
            "dont_show",
            "ext_output",
            "trt_file",
            "param_file",
            "bin_file",
            "ncnn_width",
            "ncnn_height",
            "cpu_threads",
            "expand",
            "redis_url",
            "virtual_index"
    };

    for(int i=0; i<argc;i++) {
        cout << argv[i] << " ";
    }
    cout << endl;

    for(int name_idx=0; name_idx < argc; name_idx++) {
        args[arg_names[name_idx]]=argv[name_idx];
    }

    for(pair<string, char*> set: args) {
        cout << set.first << " = " << set.second << endl;
    }

    YoloTrtProcessor yfp(atoi(args["dont_show"]),
                         atof(strtok(args["processor_arg"], "|")));
//    LandmarksProcessor lp(atoi(args["dont_show"]), atof(strtok(nullptr, "|")),
//                          atof(strtok(nullptr, "|")));


    if(args["use_filter"][0] == '1') {
        filter = new EMAFilter(atof(args["filter_arg"]));
    }
    else {
        filter = new NoneFilter();
    }

    if(args["use_out_file"][0] == '1') {
        out_file=args["out_filename"];
    }

    vector<Engine*> engines;

//    YoloDarknetEngine yfe(atoi(args["dont_show"]), atoi(args["ext_output"]),
//                          args["config_file"], args["data_file"], args["weights"],
//                          atof(args["obj_thresh"]), yfp);
//    YoloTrtEngine yte(atoi(args["dont_show"]), atoi(args["ext_output"]),
//                      args["trt_file"], args["data_file"], atof(args["obj_thresh"]), yfp);
//    YoloNcnnEngine yne(atoi(args["dont_show"]), atoi(args["ext_output"]), args["param_file"], args["bin_file"],
//                       args["data_file"], atof(args["obj_thresh"]), atoi(args["ncnn_width"]),
//                       atoi(args["ncnn_height"]), atoi(args["cpu_threads"]), yfp);
//    DlibLandmarksEngine dle(atoi(args["dont_show"]), atof(args["pix_min"]),
//                            atof(args["pix_max"]), args["face_detector"],
//                            args["shape_predictor"], *filter, atoi(args["width"]), atoi(args["height"]),
//                            lp);

    YoloTrtEngine yte(atoi(args["dont_show"]), atoi(args["ext_output"]),
                      args["trt_file"], args["data_file"], atof(args["obj_thresh"]), atoi(args["width"]), atoi(args["height"]));
    DlibLandmarksEngine dle(atoi(args["dont_show"]), atof(args["pix_min"]),
                            atof(args["pix_max"]), args["face_detector"],
                            args["shape_predictor"], *filter, atoi(args["width"]), atoi(args["height"]));

//    engines.push_back(&yfe);
    engines.push_back(&dle);
    engines.push_back(&yte);
//    engines.push_back(&yne);

    shared_ptr<Detector> p_detector;

    switch (args["input_method"][0]) {
        case '0':
            p_detector.reset(new Detector(
                    args["input"],
                    atoi(args["virtual_index"]),
                    atoi(args["dont_show"]),
                    engines,
                    args["redis_url"],
                    atoi(args["width"]),
                    atoi(args["height"]),
                    out_file
            ));
            break;
        case '1':
            p_detector.reset(new Detector(
                    atoi(args["input"]),
                    atoi(args["virtual_index"]),
                    atoi(args["dont_show"]),
                    engines,
                    args["redis_url"],
                    atoi(args["width"]),
                    atoi(args["height"]),
                    out_file
            ));
            break;
    }
    p_detector->run();
    delete filter;
    return 0;
}
