//
// Created by zhang on 2021/3/15.
//

#include "yolo_darknet_engine.h"

using namespace nlohmann;

void YoloDarknetEngine::capture(Mat& img) {
    image darknet_img = make_image(img.cols, img.rows, 3);;
    copy_image_from_bytes(darknet_img, (char*)img.data);
    darknet_image_queue.put(darknet_img);
}

void YoloDarknetEngine::inference() {
    image darknet_image = darknet_image_queue.get();
    uint64 prev = time_ns();
    vector<Prediction> object_detections = detect_image(yolo_net, class_names, darknet_image, object_thresh);
    cout << "Darknet inference time: " << double(time_ns() - prev) / 1E6 << endl;
    object_detections_queue.put(object_detections);
}

void YoloDarknetEngine::processing(promise<json> json_promise, Mat img) {
    vector<Prediction> object_detections = object_detections_queue.get();
    json_promise.set_value(json({}));
//    if(!dont_show) {
//        // Need to draw things
//        darknet_draw_boxes(object_detections, result_json, class_colors);
//    }
//    print_detections(object_detections, ext_output);
}

YoloDarknetEngine::YoloDarknetEngine(bool dont_show, bool ext_output, char* config_file, char* data_file, char* weights,
                                     double object_thresh, YoloTrtProcessor& processor) : Engine(dont_show),
                                                                                          ext_output(ext_output), object_thresh(object_thresh), processor(processor) {
    // Initialize objects detector
    NetworkConfig net_config = load_network_tools(config_file, data_file, weights, 1);
    yolo_net = net_config.net;
    class_names = net_config.class_names;
    class_colors = net_config.colors;
}


