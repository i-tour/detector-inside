//
// Created by ultcrt on 2021/3/3.
//

#include "assistant.h"
#include <array>

using namespace std;
using namespace cv;

map<char *, Scalar> generate_class_colors(const vector<char *> &classes_names) {
    map<char*, Scalar> colors;
    for(char* name: classes_names) {
        colors[name] = Scalar(random() % 256, random() % 256, random() % 256);
    }
    return colors;
}

vector<Prediction> remove_negatives(detection *dets, const vector<char *> &classes_names, int num) {
    vector<Prediction> predictions;
    for(int det_idx=0; det_idx < num; det_idx++) {
        for(int class_idx=0; class_idx < classes_names.size(); class_idx++) {
            if(dets[det_idx].prob[class_idx] > 0) {
                Prediction prediction = {classes_names[class_idx], dets[det_idx].prob[class_idx], dets[det_idx].bbox};
                predictions.push_back(prediction);
            }
        }
    }
    return predictions;
}

void print_detections(const vector<Prediction> &predictions, bool coordinates) {
    cout << "\nObjects:" << endl;
    for(const Prediction& prediction: predictions) {
        cout << prediction.name << ": "<< fixed << setprecision(2) << prediction.prob << "%";
        if(coordinates) {
            cout << "    (left_x: " << setprecision(1) << prediction.bbox.x <<"   top_y:  " << prediction.bbox.y
            << "   width:   " << prediction.bbox.w << "   height:  " << prediction.bbox.h << ")";
        }
        cout << endl;
    }
}

vector<Prediction> decode_prediction(const vector<Prediction>& predictions) {
    vector<Prediction> decoded_predictions;
    for(const Prediction& prediction: predictions) {
        Prediction decoded_prediction = {prediction.name, prediction.prob*100, prediction.bbox};
        decoded_predictions.push_back(decoded_prediction);
    }
    return decoded_predictions;
}

void darknet_draw_boxes(const vector<Prediction>& predictions, Mat image, const map<char *, Scalar> &colors) {
    for(const Prediction& prediction: predictions) {
        Rect rect = darknet_box2cv_rect(prediction.bbox);
        rectangle(image, rect, colors.find(prediction.name)->second);
        char text[sizeof(prediction.name)+20];
        sprintf(text, "Darknet %s [%.2f]", prediction.name, prediction.prob);
        putText(image, text, Point(rect.x, rect.y-5),
                FONT_HERSHEY_SIMPLEX, 0.5, colors.find(prediction.name)->second);
    }
}

Rect darknet_box2cv_rect(box bbox) {
    double x = bbox.x, y = bbox.y, w = bbox.w, h = bbox.h;
    int x_min = int(round(x - (w / 2)));
    int x_max = int(round(x + (w / 2)));
    int y_min = int(round(y - (h / 2)));
    int y_max = int(round(y + (h / 2)));
    return {Point(x_min, y_min), Point(x_max, y_max)};
}

vector<Prediction> detect_image(network *net, const vector<char *>& classes_names, image im, const double thresh,
             const double hier_thresh, const double nms) {
    int num = 0;
    int* p_num = &num;
    *p_num = 0;
    network_predict_image(net, im);
    detection* detections = get_network_boxes(
            net, im.w, im.h, float(thresh), float(hier_thresh), nullptr, 0, p_num, 0
    );
    if(nms != 0.0) {
        do_nms_sort(detections, num, classes_names.size(), float(nms));
    }
    vector<Prediction> predictions = remove_negatives(detections, classes_names, num);
    predictions = decode_prediction(predictions);
    free_detections(detections, num);
    sort(predictions.begin(), predictions.end(), [](const Prediction& a, const Prediction& b){
        return a.prob>b.prob;
    });
    return predictions;
}

Rect dlib_rect2cv_rect(dlib::rectangle rect, int ratio) {
    return {Point(rect.left()*ratio, rect.top()*ratio), Point((rect.right()+1)*ratio, (rect.bottom()+1)*ratio)};
}

vector<Point> dlib_landmarks2cv_landmarks(dlib::full_object_detection dlib_landmarks, int ratio) {
    vector<Point> cv_landmarks;
    for(int mark_idx=0; mark_idx < dlib_landmarks.num_parts(); mark_idx++) {
        cv_landmarks.emplace_back(dlib_landmarks.part(mark_idx).x()*ratio, dlib_landmarks.part(mark_idx).y()*ratio);
    }
    return cv_landmarks;
}

NetworkConfig load_network_tools(char *config_file, char *data_file, char *weight, int batch_size) {
    network* net = load_network_custom(config_file, weight, 0, batch_size);
    metadata md = get_metadata(data_file);
    vector<char*> class_names;
    for(int name_idx=0; name_idx < md.classes; name_idx++) class_names.push_back(md.names[name_idx]);
    map<char*, Scalar> colors = generate_class_colors(class_names);
    return {net, class_names, colors};
}

uint64_t time_ns() {
    return chrono::duration_cast<chrono::nanoseconds>(chrono::system_clock::now().time_since_epoch()).count();
}

dlib::rectangle darknet_bbox2dlib_rect(box bbox) {
    double tmp = 0.1;
    double x = bbox.x, y = bbox.y, w = bbox.w, h = bbox.h;
    int left = int(round(x - (w / 2)));
    int top = int(round(y - (h / 2)));
    return {int(left), int(top), int(left + w - 1), int(top + h - 1)};
}

void tkdnn_draw_boxes(const vector<tk::dnn::box> &predictions, Mat image, const vector<char *> &names,
                      const map<char *, Scalar> &colors) {
    for(const tk::dnn::box& prediction: predictions) {
        Rect rect = tkdnn_box2cv_rect(prediction);
        char* name = names[prediction.cl];
        rectangle(image, rect, colors.find(name)->second, 2);
        char text[sizeof(name)+20];
        sprintf(text, "tkDNN: %s [%.2f]", name, prediction.prob*100);
        putText(image, text, Point(rect.x, rect.y-5),
                FONT_HERSHEY_SIMPLEX, 0.5, colors.find(name)->second, 2);
    }
}

Rect tkdnn_box2cv_rect(const tk::dnn::box& bbox) {
    double x = bbox.x, y = bbox.y, w = bbox.w, h = bbox.h;
    int x_min = int(round(x));
    int x_max = int(round(x + w));
    int y_min = int(round(y));
    int y_max = int(round(y + h));
    return {Point(x_min, y_min), Point(x_max, y_max)};
}

Rect ncnn_box2dlib_rect(const float *det_res, int width, int height) {
    int x_min = int(round(det_res[2]*float(width)));
    int y_min = int(round(det_res[3]*float(height)));
    int x_max = int(round(det_res[4]*float(width)));
    int y_max = int(round(det_res[5]*float(height)));
    return {Point(x_min, y_min), Point(x_max, y_max)};
}

void ncnn_draw_boxes(const vector<NCNNDetection> &predictions, Mat image,
                     const map<char *, Scalar> &colors, double object_thresh) {
    for(const NCNNDetection& prediction: predictions) {
        if(prediction.prob >= object_thresh) {
            rectangle(image, prediction.rect, colors.find(prediction.name)->second);
            char text[sizeof(prediction.name)+20];
            sprintf(text, "NCNN: %s [%.2f]", prediction.name, prediction.prob*100);
            putText(image, text, Point(prediction.rect.x, prediction.rect.y-5),
                    FONT_HERSHEY_SIMPLEX, 0.5, colors.find(prediction.name)->second);
        }
    }
}

vector<array<double, 2>> cv_landmarks2std_landmarks(const vector<Point>& cv_landmarks, int ratio, int width, int height) {
    vector<array<double, 2>> std_landmarks;
    for(const Point_<int>& cv_landmark : cv_landmarks) {
        std_landmarks.push_back(
                array<double, 2>({
                            double(cv_landmark.x) * ratio / width,
                            double(cv_landmark.y) * ratio / height
                        })
                );
    }
    return std_landmarks;
}

