#!/bin/bash

# config_file="./res/yolo-fastest-inside.cfg"
config_file="./res/yolov4-tiny-inside.cfg"
data_file="./res/obj.data"
# weights="./res/yolo-fastest-inside.weights"
weights="./res/yolov4-tiny-inside.weights"
face_detector="./res/face_detector.dat"
shape_predictor="./res/face_landmarks.dat"
input_method=1              # input_method: 0 for string index; 1 for int index
input=0
processor_arg="0.65|0.45|0.45"
use_filter=1
filter_arg=0.4
width=1280
height=720
obj_thresh=2

pix_min=1
pix_max=150
use_out_file=1
out_filename="../test/t1.avi"
dont_show=0
ext_output=1

trt_file="./res/yolov4-tiny-inside-desktop.rt"

param_file="./res/yolov4-tiny-inside.param"
bin_file="./res/yolov4-tiny-inside.bin"
ncnn_width=416
ncnn_height=416
cpu_threads=16

expand=30

redis_url="tcp://127.0.0.1:6379"

virtual_index=19

(./build/Detector_Inside_CPP ${config_file} ${data_file} ${weights} ${face_detector} ${shape_predictor} \
  ${input_method} ${input} ${processor_arg} \
  ${use_filter} ${filter_arg} ${width} ${height} ${obj_thresh} \
  ${pix_min} ${pix_max} ${use_out_file} ${out_filename} ${dont_show} ${ext_output} ${trt_file} \
  ${param_file} ${bin_file} ${ncnn_width} ${ncnn_height} ${cpu_threads} ${expand} ${redis_url} ${virtual_index}
)
