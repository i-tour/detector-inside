#### 安装
- 自行编译libdarknet.so，并放入bin文件夹中
- 运行build.sh
- 如果报没有CUDA或AVX指令集的错，就把build.sh里的-DDLIB_USE_CUDA=1或-DUSE_AVX_INSTRUCTIONS=1删除

#### 运行
- 修改run.sh里的文件路径
- 运行run.sh
